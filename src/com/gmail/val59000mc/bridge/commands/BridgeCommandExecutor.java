package com.gmail.val59000mc.bridge.commands;

import me.dommi2212.BungeeBridge.packets.PacketRunCommand;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

import com.gmail.val59000mc.bridge.Bridge;
import com.gmail.val59000mc.bridge.i18n.I18n;

public class BridgeCommandExecutor implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) {
		
		if(sender instanceof ConsoleCommandSender && sender.hasPermission("hardcraftpvp.bridge")){
			if(args.length > 0){
				runCommand(sender, args);
			}else{
				error(sender,I18n.get("bridge.not-enough-arguments"));
			}
		}else{
			error(sender, I18n.get("bridge.sender-not-console"));
		}
		return true;
	}
	
	private void runCommand(CommandSender sender, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(Bridge.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				PacketRunCommand packet = new PacketRunCommand(StringUtils.join(args," "));
				packet.send();				
			}
		});
			
		success(sender, I18n.get("bridge.command-sent"));
	}

	private void error(CommandSender sender, String message) {
		sender.sendMessage("§c"+message);
	}

	private void success(CommandSender sender, String message) {
		sender.sendMessage("§a"+message);
	}

}
