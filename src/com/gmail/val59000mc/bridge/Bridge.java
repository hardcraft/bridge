package com.gmail.val59000mc.bridge;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.bridge.commands.BridgeCommandExecutor;
import com.gmail.val59000mc.spigotutils.Logger;

public class Bridge extends JavaPlugin{

	private static Bridge pl;
	
	public void onEnable(){
		pl = this;
		
		Logger.setColoredPrefix(ChatColor.WHITE+"["+ChatColor.GREEN+"Bridge"+ChatColor.WHITE+"]"+ChatColor.RESET+" ");
		Logger.setStrippedPrefix("[Bridge] ");
		
		getCommand("bridge").setExecutor(new BridgeCommandExecutor());
		
	}
	
	public static Bridge getPlugin(){
		return pl;
	}
	
	public void onDisable(){
	}
}
