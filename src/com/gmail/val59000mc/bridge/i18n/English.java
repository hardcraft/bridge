package com.gmail.val59000mc.bridge.i18n;

import java.util.HashMap;
import java.util.Map;

public class English {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();

		// bridge
		s.put("bridge.not-enough-arguments", "You must provide at least one argument in the command.");
		s.put("bridge.command-sent", "The command has been sent to BungeeCord");
		s.put("bridge.sender-not-console", "You are not allowed to use that command.");
				
		return s;
	}

}
