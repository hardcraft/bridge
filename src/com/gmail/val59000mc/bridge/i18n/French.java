package com.gmail.val59000mc.bridge.i18n;

import java.util.HashMap;
import java.util.Map;

public class French {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();

		// bridge
		s.put("bridge.not-enough-arguments", "Tu dois fournir au moins un argument dans la commande.");
		s.put("bridge.command-sent", "La commande a été envoyée à BungeeCord");
		s.put("bridge.sender-not-console", "Tu n'as pas le droit d'utiliser cette commande.");
		
		return s;
	}

}
